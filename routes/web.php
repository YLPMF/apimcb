<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'What are you looking for here &#x1F914;?';
});

$router->group(['prefix' => 'api'], function () use ($router) {
    
    $router->post('register', 'AuthController@register');
 
    $router->post('login', 'AuthController@login');

    $router->get('refresh', 'AuthController@refresh');

    $router->get('user', 'UserController@user');

    $router->get('users/{id}', 'UserController@singleUser');

    $router->get('all', 'UserController@all');
 
});